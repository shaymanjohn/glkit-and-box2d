//
//  ViewController.h
//  GlBox2d
//
//  Created by John on 07/08/2012.
//  Copyright (c) 2012 karmatoad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <Box2D/Box2D.h>

@interface ViewController : GLKViewController {
	b2World *world;
    
    NSMutableArray *rectangles;
    NSMutableArray *circles;
    
    double lastTime;
    
    GLKTextureInfo *circleTexture;
    bool restart;
    
    int gravityCount;
    GLfloat gravityx;
    GLfloat gravityy;
}

@property (strong, nonatomic) EAGLContext *context;

@end
