//
//  AppDelegate.h
//  GlBox2d
//
//  Created by John on 07/08/2012.
//  Copyright (c) 2012 karmatoad. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;

@end
