//
//  Circle.h
//  GlBox2d
//
//  Created by John on 10/08/2012.
//  Copyright (c) 2012 karmatoad. All rights reserved.
//

#import "Shape.h"

@interface Circle : Shape {
    GLfloat red;
    GLfloat green;
    GLfloat blue;
}

-(void)makeStatic;

@end
