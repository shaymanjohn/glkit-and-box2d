//
//  Rectangle.h
//  nibfree
//
//  Created by John Ward on 25/02/2011.
//  Copyright 2011 Karmatoad. All rights reserved.
//

#import "Shape.h"

@interface Rectangle : Shape {
	GLubyte red;
    GLubyte green;
    GLubyte blue;
	
	GLubyte squareColours[16];
}

@end
