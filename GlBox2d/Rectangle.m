//
//  Rectangle.m
//  nibfree
//
//  Created by John Ward on 25/02/2011.
//  Copyright 2011 Karmatoad. All rights reserved.
//

#import "Rectangle.h"

#define UNIT_SIZE 1.0f

@implementation Rectangle

const GLfloat squareVerts[] = {
    -0.5f, -0.5f,
     0.5f, -0.5f,
    -0.5f,  0.5f,
     0.5f,  0.5f
};

-(id)init {
	self = [super init];
    
	if (self != nil) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        int screenWidth = screenSize.height;
        int screenHeight = screenSize.width;
        
		int x = (screenWidth / 20) + rand()%(screenWidth * 9 / 10);
		int y = (screenHeight / 3) + rand()%(screenHeight * 2/ 3);
		
		int width = 5 + rand()%(screenWidth / 14);
		int height = 5 + rand()%(screenHeight / 14);
		
		if (rand()%10 > 8) {
			height = height * 4;
			width = width / 4;
		}
        
		self.centre = CGPointMake(x, y);
		self.dimensions = CGSizeMake(width, height);
		self.angle = rand()%360 / radians2degrees;
		        		
		for (int ix = 0; ix < 4; ix++) {
			red   = rand()%256;
			green = rand()%256;
			blue  = rand()%256;
			
			squareColours[(ix * 4) + 0] = red;
			squareColours[(ix * 4) + 1] = green;
			squareColours[(ix * 4) + 2] = blue;
			squareColours[(ix * 4) + 3] = 255;			
		}
		
		int hvelocity = (rand()%100) - 50;
        self.linearVel = CGPointMake(hvelocity / 1.0f, rand()%20);
		
		self.angularVel = (rand()%80) - 40;
	}
	
	return self;	
}

-(void)draw {
    glPushMatrix();
    
	glTranslatef(self.centre.x, self.centre.y, 0.0f);
	glRotatef(self.angle * radians2degrees, 0.0f, 0.0f, 1.0f);
    glScalef(self.dimensions.width, self.dimensions.height, 1.0f);

	glVertexPointer(2, GL_FLOAT, 0, squareVerts);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, squareColours);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glPopMatrix();
}

@end
