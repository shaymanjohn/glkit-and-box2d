//
//  main.m
//  GlBox2d
//
//  Created by John on 07/08/2012.
//  Copyright (c) 2012 karmatoad. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
