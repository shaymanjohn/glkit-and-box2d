//
//  Shape.m
//  GlBox2d
//
//  Created by John on 10/08/2012.
//  Copyright (c) 2012 karmatoad. All rights reserved.
//

#import "Shape.h"

@implementation Shape
@synthesize centre;
@synthesize dimensions;
@synthesize angle;
@synthesize linearVel;
@synthesize angularVel;

-(void)draw {
    NSLog(@"need to override draw method");
}

@end
