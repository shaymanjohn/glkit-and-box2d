//
//  Shape.h
//  GlBox2d
//
//  Created by John on 10/08/2012.
//  Copyright (c) 2012 karmatoad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/ES1/gl.h>
#import <Box2D/Box2D.h>

//#define PTM_RATIO 32
//#define PTM_RATIO 1

const GLfloat radians2degrees = 180.0 / M_PI;

@interface Shape : NSObject {
	CGPoint centre;
	CGSize dimensions;
	
	CGPoint linearVel;
	GLfloat angularVel;
    GLfloat angle;
}

-(void)draw;

@property (nonatomic, assign) CGPoint centre;
@property (nonatomic, assign) CGSize dimensions;
@property (nonatomic, assign) GLfloat angle;
@property (nonatomic, assign) CGPoint linearVel;
@property (nonatomic, assign) GLfloat angularVel;

@end
