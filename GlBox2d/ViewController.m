//
//  ViewController.m
//  GlBox2d
//
//  Created by John on 07/08/2012.
//  Copyright (c) 2012 karmatoad. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Rectangle.h"
#import "Circle.h"

#define NUM_RECTS   40
#define NUM_CIRCLES 60
#define GRAVITY     -9.81f

@implementation ViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
    
    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    [self setPreferredFramesPerSecond:60];
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    
    [EAGLContext setCurrentContext:self.context];
    
    glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
    
    CGSize size = [[UIScreen mainScreen] bounds].size;
    
    glOrthof(0.0f, size.height, 0.0f, size.width, 1.0f, -1.0f);
	glViewport(0, 0, size.height, size.width);
    
    glDisable(GL_DEPTH_TEST);
	glMatrixMode(GL_MODELVIEW);
    
    glEnableClientState(GL_VERTEX_ARRAY);
    
    NSDictionary *options = [NSMutableDictionary dictionaryWithObjects:[NSMutableArray arrayWithObjects:[NSNumber numberWithBool:NO], nil]
                                                               forKeys:[NSMutableArray arrayWithObjects:GLKTextureLoaderOriginBottomLeft, nil]];
    
    NSError *error = nil;
    CGImageRef cgImage = [UIImage imageNamed:@"particle.png"].CGImage;
    circleTexture = [GLKTextureLoader textureWithCGImage:cgImage options:options error:&error];
    
    if (error) {
        NSLog(@"%@", [error description]);
    }
    
    rectangles = [[NSMutableArray alloc] init];
    circles = [[NSMutableArray alloc] init];
    
	[self createWorld];
	[self initShapes];
    
    lastTime = CACurrentMediaTime();
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapped)];
    doubleTap.numberOfTapsRequired = 3;
    
    [self.view addGestureRecognizer:doubleTap];
}

-(void)viewDidUnload {
    [super viewDidUnload];
    
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
    
	self.context = nil;
}

-(void)doubleTapped {
    restart = true;
}

-(void)setGravity {
    return;
    
    switch(gravityCount) {
        case 0:
            gravityx = 0.0f;
            gravityy = GRAVITY;
            break;
            
        case 1:
            gravityx = GRAVITY;
            gravityy = 0.0f;
            break;
            
        case 2:
            gravityx = 0.0f;
            gravityy = GRAVITY;
            break;
            
        case 3:
            gravityx = GRAVITY;
            gravityy = 0.0f;
            break;
            
        default:
            break;
    }
    
	b2Vec2 worldGravity(gravityx, gravityy);
    world->SetGravity(worldGravity);
}

-(void)createWorld {
	CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    CGFloat width = screenSize.height;
    CGFloat height = screenSize.width;
    
    gravityCount = 0;
    
    b2Vec2 worldGravity(0, GRAVITY);
    world = new b2World(worldGravity);
    world->SetAllowSleeping(false);
	world->SetContinuousPhysics(true);
	
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0, 0);
	
	b2Body *groundBody = world->CreateBody(&groundBodyDef);
	
	b2EdgeShape groundBox;
    
    b2Vec2 blCorner = b2Vec2(0, 0);
    b2Vec2 brCorner = b2Vec2(width, 0);
    b2Vec2 tlCorner = b2Vec2(0, height);
    b2Vec2 trCorner = b2Vec2(width, height);
    
    groundBox.Set(blCorner, brCorner);
    groundBody->CreateFixture(&groundBox, 0);
    
    groundBox.Set(tlCorner, trCorner);
    groundBody->CreateFixture(&groundBox, 0);
    
    groundBox.Set(tlCorner, blCorner);
    groundBody->CreateFixture(&groundBox, 0);
    
    groundBox.Set(trCorner, brCorner);
    groundBody->CreateFixture(&groundBox, 0);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    gravityCount++;
    if (gravityCount == 4) {
        gravityCount = 0;
    }
    
    [self setGravity];
}

-(void)initShapes {
    restart = false;
    
    if (rectangles.count > 0) {
        [rectangles removeAllObjects];
    }
    
    if (circles.count > 0) {
        [circles removeAllObjects];
    }
    
	srand(time(NULL));
    
	for (int ix = 0; ix < NUM_RECTS; ix++) {
		Rectangle *thisRectangle = [[Rectangle alloc] init];
        
		[rectangles addObject:thisRectangle];
		[self addPhysicalRectangle:thisRectangle];
	}
    
    for (int ix = 0; ix < NUM_CIRCLES; ix++) {
        Circle *thisCircle = [[Circle alloc] init];
        
        [circles addObject:thisCircle];
        [self addPhysicalCircle:thisCircle];
    }
}

-(void)addPhysicalRectangle:(Rectangle *)thisRect {
	b2BodyDef bodyDef;
    
	bodyDef.type = b2_dynamicBody;
	
	bodyDef.position.Set(thisRect.centre.x, thisRect.centre.y);
	bodyDef.angle = thisRect.angle;
	bodyDef.userData = (__bridge void *)thisRect;
    
	bodyDef.linearVelocity = b2Vec2(thisRect.linearVel.x, thisRect.linearVel.y);
	bodyDef.angularVelocity = thisRect.angularVel;
    
	b2Body *body = world->CreateBody(&bodyDef);
	
	b2PolygonShape dynamicBox;
	CGPoint boxDimensions = CGPointMake(thisRect.dimensions.width / 2.0, thisRect.dimensions.height / 2.0);
	dynamicBox.SetAsBox(boxDimensions.x, boxDimensions.y);
    
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;
	fixtureDef.restitution = 0.5f;
	body->CreateFixture(&fixtureDef);
}

-(void)addPhysicalCircle:(Circle *)thisCircle {
	b2BodyDef bodyDef;
    
	bodyDef.type = b2_dynamicBody;
	
	bodyDef.position.Set(thisCircle.centre.x, thisCircle.centre.y);
	bodyDef.angle = thisCircle.angle;
	bodyDef.userData = (__bridge void *)thisCircle;
    
	bodyDef.linearVelocity = b2Vec2(thisCircle.linearVel.x, thisCircle.linearVel.y);
	bodyDef.angularVelocity = thisCircle.angularVel;
    
    if (rand()%10 == 1) {
        bodyDef.type = b2_staticBody;
        bodyDef.linearVelocity = b2Vec2(0, 0);
        bodyDef.angularVelocity = 0.0;
        [thisCircle makeStatic];
    }
    
	b2Body *body = world->CreateBody(&bodyDef);
	
    b2CircleShape circleShape;
    circleShape.m_radius = thisCircle.dimensions.width / 2.0;
    
	b2FixtureDef fixtureDef;
    fixtureDef.shape = &circleShape;
	fixtureDef.density = 0.5f;
	fixtureDef.friction = 0.2f;
	fixtureDef.restitution = 0.6f;
	body->CreateFixture(&fixtureDef);
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

-(void)tearDownGL {
    [EAGLContext setCurrentContext:self.context];
}

-(void)update {
	int32 velocityIterations = 8;
	int32 positionIterations = 3;
    
	double currentTime = CACurrentMediaTime();
    double timeDelta = currentTime - lastTime;
    lastTime = currentTime;
	
	world->Step(timeDelta, velocityIterations, positionIterations);
	
    b2Body *thisBody = world->GetBodyList();
    
    while (thisBody) {
        Shape *thisShape = (__bridge Shape *)thisBody->GetUserData();
        
		if (thisShape != NULL) {
			CGPoint newCenter = CGPointMake(thisBody->GetPosition().x, thisBody->GetPosition().y);
			thisShape.centre = newCenter;
			thisShape.angle = thisBody->GetAngle();
		}
        
        thisBody = thisBody->GetNext();
    }
    
	if (restart) {
		[self restart];
	}
}

-(void)restart {
	delete world;
    
	[self createWorld];
	[self initShapes];
}

-(void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
    glClearColor(0.1f, 0.1f, 0.4f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
	glLoadIdentity();
    
    glEnableClientState(GL_COLOR_ARRAY);
	for (Rectangle *thisRect in rectangles) {
		[thisRect draw];
	}
    glDisableClientState(GL_COLOR_ARRAY);
    
    glEnable(GL_TEXTURE_2D);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glBindTexture(GL_TEXTURE_2D, circleTexture.name);
    
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    for (Circle *thisCircle in circles) {
        [thisCircle draw];
    }
    
    glDisable(GL_BLEND);
    glDisable(GL_TEXTURE_2D);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

@end




