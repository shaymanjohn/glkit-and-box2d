//
//  Circle.m
//  GlBox2d
//
//  Created by John on 10/08/2012.
//  Copyright (c) 2012 karmatoad. All rights reserved.
//

#import "Circle.h"

@implementation Circle

static const GLfloat gRectVertexData[] = {
    -0.5f, -0.5f,
     0.5f, -0.5f,
    -0.5f,  0.5f,
     0.5f,  0.5f
};

static const GLfloat gRectTextureData[] = {
    0, 1,
    1, 1,
    0, 0,
    1, 0
};

-(id)init {
	self = [super init];
    
	if (self != nil) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        int screenWidth = screenSize.height;
        int screenHeight = screenSize.width;
        
		int x = (screenWidth / 20) + rand()%(screenWidth * 9 / 10);
		int y = (screenHeight / 3) + rand()%(screenHeight * 2/ 3);
		
		int width = 5 + rand()%(screenWidth / 14);
        
		self.centre = CGPointMake(x, y);
		self.dimensions = CGSizeMake(width, 0);
        
		self.angle = rand()%360 / radians2degrees;
        
        red   = (128 + rand()%128) / 256.0;
        green = (128 + rand()%128) / 256.0;
        blue  = (128 + rand()%128) / 256.0;
		
		int hvelocity = (rand()%100) - 50;
        self.linearVel = CGPointMake(hvelocity / 1.0f, rand()%20);
		
		self.angularVel = (rand()%80) - 40;
    }
    
    return self;
}

-(void)makeStatic {
    red = 1.0f;
    green = 1.0f;
    blue = 1.0f;
    self.dimensions = CGSizeMake(40, 0);
}

-(void)draw {
    glPushMatrix();
    
	glVertexPointer(2, GL_FLOAT, 0, gRectVertexData);
	glTexCoordPointer(2, GL_FLOAT, 0, gRectTextureData);
    
    glColor4f(red, green, blue, 1.0f);
    
	glTranslatef(self.centre.x, self.centre.y, 0.0f);
	glRotatef(self.angle * radians2degrees, 0.0f, 0.0f, 1.0f);
    glScalef(self.dimensions.width * 2, self.dimensions.width * 2, 1.0f);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glPopMatrix();
}

@end
